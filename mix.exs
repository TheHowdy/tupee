defmodule Tupee.MixProject do
  use Mix.Project

  def project do
    [
      app: :tupee,
      version: "0.1.0",
      elixir: "~> 1.10",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  def application do
    [
      extra_applications: [:logger, :memento, :httpoison],
      mod: {Tupee.Application, []}
    ]
  end

  defp deps do
    [
      {:nostrum, git: "https://github.com/Kraigie/nostrum.git"}, # Discord Library
      {:memento, "~> 0.3.1"}, # Convenient Mnesia API
      {:jason, "~> 1.2"}, # JSON API (need for charting)
      {:httpoison, "~> 1.6"}, # Needed for calling external APIs
      {:tzdata, "~> 1.0.3"}, # Timezone DB
      {:credo, "~> 1.4", only: [:dev, :test], runtime: false} # Linting
    ]
  end
end
