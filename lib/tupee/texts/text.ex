defmodule Tupee.Text do
  @moduledoc false

  @texts %{
    command_status: [
      "What a glorious day for capitalism! :money_mouth:",
      "Let's make some money! :moneybag:",
      "What is? Time's money! :money_with_wings:",
      "I'm online. That makes 5HZT.:face_with_monocle:",
      "I'm alright comrade. Wait. Wrong ideology.:no_mouth: ",
      "Psst.. Wanna buy some oil? :spy: :eyes::flag_us:",
      "Hey, what's cheaper than this joke? The salary of my workers.:laughing:"
    ],

    syntax_wrong_length: [
      "You probably didn't read the syntax for this command, let me get that for you:\n`$help`",
      "Did you try reading the syntax?\n`$help`",
      "Read this and give it a new try :point_right:\n`$help`",
      "Keep searching boys, we gonna find why you can't remember the command syntax:\n`$help`",
      "I can assist you with a paid subscription service, or this:\n`$help`(I prefer the former)"
    ],

    syntax_errors: [
      &Tupee.Texts.Errors.handle_command_errors/1
    ],

    total_spam_protection: [

    ],

    user_spam_protection: [

    ],

    command_pay_not_enough_money: [
      "Nice money you have there. :clap: Try coming back with enough of it next time.",
      "You have not enough money. Just a tiny-tiny bit, ya know. :pinching_hand:",
      "I need :dollar:, :dollar:, :dollar: would I need (_hey hey_).",
      "Hit the road $user, don't you come back with enough money no more, no more, no more, no more.:blue_car:",
    ],

    command_pay_recipient_not_found: [
      "Try to learn how to use auto-completion, than repeat this command.:keyboard:",
      """
      Roses are red,
      Violets are blue,
      Your recipient does not exist,
      So you have to try again, too.
      """,
      """
      Trying :iphone:... :Trying :telephone:... Trying :pager:... Trying :fax:... Trying :postal_horn:...
      Nope, could not find your recipient.
      """
    ],

    command_pay_recipient_bot: [
      "It's great that you want to give money to a bot. But I'm sure he doesn't need it.:face_with_monocle:",
      "Experimenting, eh? No, you can't give a bot money.:robot:",
      """
      Careful what you wish,
      Careful what you say,
      Careful what you wish, you may regret it,
      **Careful what you wish, you may just get it.**:eye_in_speech_bubble:

      (An epic way to say you can't pay money to a bot.)
      """
    ],

    command_pay_sender_bot: [
      "**Robot:** Beep beep, boop, beep boop.\n**English:** No.",
      "Payment successful. No just kidding.:laughing:",
      "Nice try. Try again tomorrow."
    ],

    command_pay_success: [
      "Payment successful. Look at all the money! :money_mouth:",
      "It worked. Have fun with it. :video_game:",
      "Money, money, gimme your :moneybag:! Oh... you gave it to him.",
      "Success. Really, it worked. :raised_hands:",
      "Worked... We should really introduce taxes here...",
      "Success.. Do you know what makes me sad? If I ain't getting a cent."
    ],

    command_pay_recipient_self: [
      """
      You apparently suffer from a dual personality.:bearded_person:
      That's ok, but you have to create a second discord account for it.
      """,
      "Trying to be big-:brain:, ain't ya?",
      "",
      "Successfully paid `500000HZT` to Santa Claus:santa:."
    ],

    command_buy_success: [
      "Successful, you paid `$amount $code`.:money_mouth:",
      "Success, `$amount $code` have withdrawn from your account and converted.",
      "Money, money, money, your `$amount $code` were just converted!:laughing:"
    ],

    command_buy_sender_bot: [
      "**Robot:** Beep beep, boop, beep boop.\n**English:** No.",
      "Buy successful. No just kidding.:laughing:",
      "Nice try. Try again tomorrow.:dollar:"
    ],

    command_buy_not_enough_money: [
      "Nice money you have there. :clap: Try coming back with enough of it next time.",
      "You have not enough money. Just a tiny-tiny bit, ya know. :pinching_hand:"
    ]
  }

  def get_random_text(id, params \\ []) do
    @texts[id]
    |> Enum.random()
    |> handle_text(params)
  end

  defp handle_text(text, params) when is_bitstring(text) do
    Enum.reduce(params, text, fn {pattern, replacement}, text ->
      String.replace(text, "$#{pattern}", "#{replacement}")
    end)
  end

  defp handle_text(gen, params) when is_function(gen) do
    gen.(params)
  end
end
