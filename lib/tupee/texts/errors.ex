defmodule Tupee.Texts.Errors do
  @moduledoc false

  defp normalize_errors(params) do
    Enum.map(params, fn {arg, syntax} ->
      case syntax do
        :currency ->
          "`#{arg}` is a currency. " <>
          if Tupee.Util.Syntax.valid_code?(arg) do
            "It doesn't seem to be a valid currency code and if it's a currency name, it doesn't exist."
          else
            "The currency doesn't exist."
          end
        :user ->
          "#{arg} is a mentioned user."
        :full_currency ->
          "`#{arg}` is a full currency name. An example for such would be `Hölzlthaler(HZT)`."
        :amount ->
          "`#{arg}` is a positive amount with a maximum of 5 decimal places. Allowed are e.g. `3`, or `20323.45`."
        str when is_bitstring(str) ->
          "`#{arg}` and `#{str}` are the same thing. `#{str}` should be there instead."
      end
    end)
  end

  defp compose_error_message(msg, first_message? \\ false) do
    suggestions = [
      "I don't believe that",
      "I don't think that",
      "I don't think you're stupid, but never",
      "I know I'm just a bot, but i don't think that",
      "I could be wrong, but I'm sure you are. Never on earth"
    ]
    suggestion = Enum.random(suggestions)

    openings = ["And,", "And also,", "Also,", "Besides,"]
    opening = if first_message? do "" else Enum.random(openings) end

    ":point_right: #{opening} #{suggestion} #{msg}\n\n"
  end

  def handle_command_errors(params) do
    text = "You've got some errors there, e.g.\n\n"

    errors = normalize_errors(params)

    [first | errors] = errors
    first_msg = compose_error_message(first, true)

    msg = Enum.reduce(errors, text <> first_msg, fn msg, acc ->
      acc <> compose_error_message(msg)
    end)

    random_endings = [
      ":cookie: Cookies are great.",
      ":ok_hand: I won't improve your salary, don't worry.",
      ":robot: If you think I'm wrong, you're wrong.",
      ":pinching_hand: Try appreciating this message, just a tiny bit.",
      ":metal: Listen to some good music. Really, it helps with this messages.",
      ":coffee: Get yourself a coffee and keep going."
    ]
    msg <> Enum.random(random_endings)
  end
end
