defmodule Tupee.Economy do
  @moduledoc false
  alias Tupee.Economy.{Balance,Rate}

  def convert(user, tgt_amount, source_currency, target_currency) do
    if user.bot() do
        {:error, :bot_sender}
    else
        Balance.give_base_currency_if_needed(user)

        src_code = source_currency.code
        tgt_code = target_currency.code

        market = capture_market_situation_for(src_code, tgt_code)

        src_rate = Rate.rate_of(src_code)
        tgt_rate = Rate.rate_of(tgt_code)

        src_amount = tgt_amount * tgt_rate / src_rate

        case Balance.add(user.id, src_code, -src_amount) do
          {:ok, _} ->
            Balance.add(user.id, tgt_code, tgt_amount)
            adjust_rates(src_code, tgt_code, market)
            {:ok, src_amount}
          other ->
            other
        end
    end
  end

  def pay(src_user, tgt_user, currency, amount) do
    cond do
      src_user.bot() ->
        {:error, :bot_sender}
      tgt_user.bot() ->
        {:error, :bot_recipient}
      src_user.id() == tgt_user.id() ->
        {:error, :self_recipient}
      true ->
        Balance.give_base_currency_if_needed(src_user)
        Balance.give_base_currency_if_needed(tgt_user)
        transfer(src_user.id(), tgt_user.id(), currency.code, amount)
    end
  end

  defp transfer(src_id, tgt_id, code, amount) do
    with {:ok, _} <- Balance.add(src_id, code, -amount),
         {:ok, _} <- Balance.add(tgt_id, code, amount)
    do
      :ok
    else
      err -> err
    end
  end

  defp capture_market_situation_for(code_a, code_b) do
    market_values = Balance.get_market_values()
    share_a = Balance.get_market_value_percentage(code_a, market_values)
    share_b = Balance.get_market_value_percentage(code_b, market_values)
    {share_a, share_b}
  end

  defp adjust_rates(code_a, code_b, {old_share_a, old_share_b}) do
    {share_a, share_b} = capture_market_situation_for(code_a, code_b)

    adjust_a = share_a - old_share_a
    adjust_b = share_b - old_share_b

    Rate.adjust_rate_by_percentage(code_a, adjust_a)
    Rate.adjust_rate_by_percentage(code_b, adjust_b)
  end
end
