defmodule Tupee.Economy.Balance do
  alias Memento.Query
  alias Tupee.Economy.Rate
  alias Tupee.Economy.Provider.CurrencyProvider
  alias Tupee.Util

  import Tupee.Economy.Transaction

  use Memento.Table,
      attributes: [:user_code, :balance]

  def get_balance(user, code) do
    transaction do
      CurrencyProvider.recv_update(code)

      __MODULE__
      |> Query.read({user, code})
      |> this_or_new(user, code)
    end
  end

  def get_balances(user) do
    transaction do
      CurrencyProvider.recv_all_updates()

       __MODULE__
       |> Query.match({{user, :_}, :_})
       |> Enum.filter(fn %__MODULE__{balance: balance} -> balance != 0 end)
    end
  end

  def add(user, code, amount) do
    transaction do
      CurrencyProvider.recv_update(code)

      balance =
        __MODULE__
        |> Query.read({user, code})
        |> this_or_new(user, code)

      tgt_amount = balance.balance + amount

      if tgt_amount < 0 do
        {:error, :not_enough_money}
      else
        balance = %{balance | balance: tgt_amount}

        Query.write(balance)
        CurrencyProvider.send_update(code, balance)

        {:ok, tgt_amount}
      end
    end
  end

  def get_market_worth do
    transaction do
      CurrencyProvider.recv_all_updates()
      rates = Rate.get_current_rates()

       Query.all(__MODULE__) |> Enum.reduce(%{},
        fn (%__MODULE__{user_code: {_, code}, balance: balance}, acc) ->
          rate = rates[code]
          Map.update(acc, code, balance * rate, &(balance * rate + &1))
      end)
    end
  end

  def get_market_value_percentage(code, values \\ nil) do
    values = values || get_market_values();

    values[code] / values[:total]
  end

  def give_base_currency_if_needed(user) do
    transaction do
      length = get_balances(user.id()) |> length()

      if length == 0 and !user.bot() do
        currency = Util.get_base_currency_code()
        amount = Util.get_starter_currency_amount()
        add(user.id(), currency, amount)
      end
    end
  end

  defp this_or_new(res, user, code) do
    if res do
      res
    else
      %__MODULE__{user_code: {user, code}, balance: 0}
    end
  end
end
