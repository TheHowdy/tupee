defmodule Tupee.Economy.Currency do
  alias Memento.Query
  alias Tupee.Util
  alias Tupee.Util.Syntax
  alias Tupee.Economy.Rate

  import Tupee.Economy.Transaction

  use Memento.Table,
      attributes: [:code, :name, :base_currency?]

  def get_currency(name) do
    transaction do
      cond do
        Syntax.valid_code?(name) ->
          Query.read(__MODULE__, name)

        Syntax.valid_full_currency_name?(name) ->
          regex = ~r/^\p{L}+\(([A-Z]{3})\)$/u
          abbr = Regex.run(regex, name, capture: :all_but_first)
          get_currency(abbr)

        true ->
          Query.match(__MODULE__, {:_, name, :_})
          |> List.first()
      end
    end
  end

  def get_currency_codes do
      get_currencies
      |> Enum.map(fn %__MODULE__{code: code} -> code end)
  end

  defp get_currencies do
    transaction do
      Query.all(__MODULE__)
    end
  end

  def get_base_currency do
    transaction do
      Query.match(__MODULE__, {:_, :_, true})
      |> List.first()
    end
  end

  def create_currency(code, name, rate, base_currency? \\ false) do
    currency = %__MODULE__{code: code, name: name, base_currency?: base_currency?}

    transaction do
      Query.write(currency)
      Rate.add_rate(code, rate)
    end
  end

  def create_base_currency do
    name = Util.get_base_currency()
    code = Util.get_base_currency_code()

    create_currency(code, name, 1, true)
  end
end
