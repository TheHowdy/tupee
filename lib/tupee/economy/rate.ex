defmodule Tupee.Economy.Rate do
  alias Memento.Query
  alias Tupee.Economy.Currency

  import Tupee.Economy.Transaction

  use Memento.Table,
      attributes: [:id, :timestamp, :code, :rate],
      type: :ordered_set,
      index: [:code],
      autoincrement: true

  def rate_of(code, _ \\ [])

  def rate_of(code, [timestamp: %DateTime{} = timestamp]) do
    timestamp = DateTime.to_unix(timestamp)
    rate_of(code, timestamp: timestamp)
  end

  def rate_of(code, [timestamp: timestamp]) do
    transaction do
      __MODULE__
      |> Query.select([{:==, :code, code}, {:<, :timestamp, timestamp}])
      |> List.first()
      |> to_rate()
    end
  end

  def rate_of(code, []) do
    transaction do
      __MODULE__
      |> Query.match({:_, :_, code, :_})
      |> List.first()
      |> to_rate()
    end
  end

  defp to_rate(%__MODULE__{rate: rate}) do rate end
  defp to_rate(_) do 0 end

  def get_current_rates do
    transaction do
      Currency
      |> Query.all()
      |> Enum.map(fn %Currency{code: code} -> code end)
      |> Enum.reduce(%{}, fn (code, acc) ->
        Map.put(acc, code, rate_of(code))
      end)
    end
  end

  def add_rate(code, rate) do
    transaction do
      timestamp = DateTime.utc_now() |> DateTime.to_unix()

      Query.write(%__MODULE__{code: code, rate: rate, timestamp: timestamp})
    end
  end

  def adjust_rate_by_percentage(code, percentage) do
    transaction do
      rate = rate_of(code)
      add_rate(code, rate + rate * percentage)
    end
  end

  defp date_of(%__MODULE__{timestamp: timestamp}) do
    DateTime.from_unix!(timestamp)
  end
end
