defmodule Tupee.Economy.Provider.CoinProvider do
  @moduledoc false
  alias Tupee.Economy.Provider.CurrencyProvider
  alias Tupee.Economy.{Balance, Rate}
  alias Nostrum.Query

  import Tupee.Economy.Transaction

  @behaviour CurrencyProvider

  @url "https://fish-server.org/3bibot/api.php"
  @code "COI"

  @impl CurrencyProvider
  def recv_update() do
    case send_update_request() do
      {:ok, %HTTPoison.Response{body: "wrong api key"}} ->
        {:error, :internal}

      {:ok, %HTTPoison.Response{body: body}} ->
        body
        |> Jason.decode!()
        |> to_balances()
        |> override_db()
        :ok

      {:error, _} ->
        {:error, :unavailable}
    end
  end

  @impl CurrencyProvider
  def send_update(%Balance{balance: balance}) do
    # TODO Implement this
  end

  defp to_balances(json) do
    Enum.map(json, fn {userid, %{"coins" => coins}} ->
      %Balance{user_code: {userid, @code}, balance: coins}
    end)
  end

  defp override_db(balances) do
    transaction do
      Enum.each(&(Query.write(&1))) # TODO Handle errors here
    end
  end

  defp send_update_request do
    api_key = Application.get_env(:tupee, :coin_api_key)
    body = "key=#{api_key}"
    header = [{"Content-Type", "application/x-www-form-urlencoded"}]
    res = HTTPoison.post(@url, body, header)
  end
end
