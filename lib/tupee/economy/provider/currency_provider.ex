defmodule Tupee.Economy.Provider.CurrencyProvider do
  @moduledoc false
  alias Tupee.Economy.Balance
  alias Tupee.Util

  @typep result() :: :ok | {:error, :unavailable} | {:error, :internal}

  @typep data() :: Balance

  @callback recv_update() :: result()

  @callback send_update(data()) :: result()

  def recv_update(code) do
    case Util.get_currency_providers() do
      %{^code => module} -> module.recv_update()
      _ -> :ignored
    end
  end

  def recv_all_updates do
    Util.get_currency_providers()
    |> Map.values()
    |> Enum.each(&(&1.recv_update()))
  end

  def send_update(code, data) do
    case Util.get_currency_providers() do
      %{^code => module} -> module.send_update(data)
      _ -> :ignored
    end
  end
end
