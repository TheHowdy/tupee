defmodule Tupee.Economy.Transaction do
  @moduledoc false

  defmacro transaction(do: expression) do
    quote do
      if Memento.Transaction.inside?() do
        unquote(expression)
      else
        {:ok, value} = Memento.transaction(fn -> unquote(expression) end) # TODO Handle this
        value
      end
    end
  end
end
