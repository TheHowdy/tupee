defmodule Tupee.Chart do
  alias Tupee.Economy.{Balance,Rate,Currency}

  @one_hour 60 * 60

  @one_day 60 * 60 * 24

  @months [
    "January", "February", "March",
    "April", "May", "June",
    "July", "August", "September",
    "October", "November", "December"
  ]

  # TODO handle beeing out-of-colors
  @colors [
    "AliceBlue", "CornflowerBlue", "Beige", "Brown", "Aquamarine",
    "BlueViolet", "BurlyWood", "CadetBlue", "AntiqueWhite", "Crimson"
  ]

  def plot_economy(:day) do
    DateTime.utc_now()
    |> DateTime.add(@one_hour * -24) # -24hours
    |> plot_economy(@one_hour, day_labels())
  end

  def plot_economy(:month) do
    DateTime.utc_now()
    |> DateTime.add(@one_day * -30) # -30days
    |> plot_economy(@one_day, month_labels())
  end

  def plot_economy(start_datetime, time_split, labels) do
    currencies = Currencies.get_currency_names()
    count = length(labels)

    rates = for code <- currencies, n <- 1..count do
      time = DateTime.add(start_datetime, time_split * n)
      {code, Rate.rate_of(code, timestamp: time)}
    end

    rates = Enum.reverse(rates)

    data = Enum.reduce(rates, %{}, fn ({code, rate}, map) ->
      Map.update(map, code, [rate], fn old -> [rate | old] end)
    end)

    line_chart(labels, data)
  end

  def plot_market_worth do
    Balance.get_market_worth() |> pie_chart(data)
  end

  defp line_chart(labels, data) do
    count = Map.values(data) |> length()

    datasets =
      0..count
      |> Enum.zip(data)
      |> Enum.map(fn {n, {key, values}} ->
        %{
          "label" => key, "data" => values,
          "fill" => false, "borderColor" => color_at(n),
          "borderCapStyle" => "round"
        }
      end)

    %{
      "type" => "line",
      "data" => %{
        "labels" => labels,
        "datasets" => datasets
      }
    }
    |> Jason.encode!()
    |> gen_url()
  end

  defp pie_chart(data) do
    %{
      "type" => "doughnut",
      "data" => %{
        "labels" => Map.keys(data),
        "datasets" => [%{"data" => Map.values(data)}]
      }
    }
    |> Jason.encode!()
    |> gen_url()
  end

  defp gen_url(json) do
    "https://quickchart.io/chart?c=#{URI.encode(json)}" # TODO URI.encode is kinda bug-ed here
  end

  defp color_at(index) do
    Enum.at(@colors, index)
  end

  defp shift_timezone(date) do
    DateTime.shift_zone!(date, Tupee.Util.get_default_timezone())
  end

  defp month_labels do
    now = DateTime.utc_now()

    to_label = fn n ->
      new_date = DateTime.add(now, -n * @one_day)
      new_date = shift_timezone(new_date)
      "#{new_date.day()}. #{Enum.at(@months, new_date.month())}"
    end

    Enum.map(30..0, to_label)
  end

  defp day_labels do
    now = DateTime.utc_now()

    to_label = fn n ->
      new_date = DateTime.add(now, -n * @one_hour)
      new_date = shift_timezone(new_date)
      "#{new_date.hour()}:00"
    end

    Enum.map(23..0, to_label)
  end
end
