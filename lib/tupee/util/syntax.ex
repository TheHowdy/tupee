defmodule Tupee.Util.Syntax do
  @moduledoc false
  alias Tupee.Util
  alias Tupee.Economy.Currency
  alias Tupee.Commands.CommandManager

  defmacrop if_valid(valid?, tuple, do: expression) do
    quote do
      if(unquote(valid?)) do
        {:ok, unquote(expression)}
      else
        {:error, unquote(tuple)}
      end
    end
  end

  def check_syntax(msg) do
    with true <- valid_command?(msg),
         [command | args] <- split_command(msg),
         syntax when not is_nil(syntax) <- CommandManager.syntax_definition(command)
    do
      check_syntax_internal(command, args, syntax)
    else
      false -> {:error, :not_a_command}
      nil -> {:error, :command_not_found}
      _ -> {:error, :internal_error}
    end
  end

  def check_syntax_internal(command, args, syntax) do
    args = Enum.flat_map(args, fn arg ->
      if valid_money_amount?(arg) do
        amount = get_amount_part(arg)
        currency = get_currency_part(arg)
        [amount, currency]
      else
        [arg]
      end
    end)

    processed =
      Enum.zip(args, syntax)
      |> Enum.map(&process_syntax_pair/1)

    errors =
      Enum.filter(processed, &failed?/1)
      |> Enum.map(fn {:error, tup} -> tup end)

    cond do
      (length args) != (length syntax) -> {:error, {:wrong_length, command}}
      (length errors) != 0 -> {:error, {:errors, errors}}
      true ->
         args =
           Enum.filter(processed, &succeded?/1)
           |> Enum.map(fn {:ok, arg} -> arg end)
         {:ok, {command, args}}
    end
  end

  def get_currency_part(full_currency) do
    String.slice(full_currency, -3..-1)
  end

  def get_amount_part(full_currency) do
    String.slice(full_currency, 0..-4)
  end

  defp process_syntax_pair({arg, :amount} = tup) do
    valid? = valid_amount?(arg)
    if_valid valid?, tup do
      {fp, ""} = Float.parse(arg)
      fp
    end
  end

  defp process_syntax_pair({arg, :currency} = tup) do
    {valid?, currency} = validate_currency(arg)
    if_valid valid?, tup do currency end
  end

  defp process_syntax_pair({arg, :user} = tup) do
    valid? = String.match?(arg, ~r/^<@![0-9]+>$/)
    if_valid valid?, tup do arg end
  end

  defp process_syntax_pair({arg, :full_currency} = tup) do
    valid_syntax? = valid_full_currency_name?(arg)

    currency_part = get_currency_part(arg)
    {valid?, currency} = validate_currency(currency_part)

    if_valid (valid_syntax? and valid?), tup do currency end
  end

  defp process_syntax_pair({arg, str} = tup) when is_bitstring(str) do
    valid? = String.downcase(arg) == String.downcase(str)

    if_valid valid?, tup do arg end
  end

  def valid_code?(code) do
    String.match?(code, ~r/^[A-Z]{3}$/)
  end

  def valid_amount?(amount) do
    with true <- String.match?(amount, ~r/^[0-9]+(\.[0-9]{1,5})?$/),
         {n, ""} when n > 0 <- Float.parse(amount)
    do
      true
    else
      _ -> false
    end
  end

  def valid_money_amount?(str) do
    String.match?(str, ~r/^[0-9]+(\.[0-9]+)?[A-Z]{3}$/)
  end

  def valid_full_currency_name?(str) do
    String.match?(str, ~r/^\p{L}+\([A-Z]{3}\)$/u)
  end

  def valid_command?(msg) do
    String.starts_with?(msg, Util.get_prefix())
  end

  defp validate_currency(arg) do
    case Currency.get_currency(arg) do
      nil -> {false, nil}
      currency -> {true, currency}
    end
  end

  defp succeded?({:ok, _}) do true end
  defp succeded?({:error, _}) do false end
  defp failed?(tup) do !succeded?(tup) end

  defp split_command(msg) do
    prefix = Util.get_prefix()
    content = String.replace_prefix(msg, prefix, "")
    String.split(content)
  end
end
