defmodule Tupee.Util do
  @moduledoc false

  @colors %{
    default: 0,
    aqua: 1752220,
    green: 3066993,
    blue: 3447003,
    purple: 10181046,
    gold: 15844367,
    orange: 15105570,
    red: 15158332,
    grey: 9807270,
    darker_grey: 8359053,
    navy: 3426654,
    dark_aqua: 1146986,
    dark_green: 2067276,
    dark_blue: 2123412,
    dark_pruple: 7419530,
    dark_gold: 12745742,
    dark_orange: 11027200,
    dark_red: 10038562,
    dark_grey: 9936031,
    light_grey: 12370112,
    dark_navy: 2899536,
    vivid_pink: 16580705,
    dark_vivid_pink: 12320855
  }

  def color(color) do
    @colors[color]
  end

  def get_prefix do
    Application.get_env(:tupee, :prefix)
  end

  def get_base_currency_code do
    Application.get_env(:tupee, :base_currency_code)
  end

  def get_base_currency do
    Application.get_env(:tupee, :base_currency)
  end

  def get_starter_currency_amount do
    Application.get_env(:tupee, :starter_currency_amount)
  end

  def get_currency_providers do
    Application.get_env(:tupee, :currency_providers)
  end

  def get_default_timezone do
    Application.get_env(:tupee, :timezone)
  end

  def get_bot_avatar do
    Nostrum.Struct.User.avatar_url(Nostrum.Cache.Me.get())
  end

  def get_rate_limit_tick_sec do
    Application.get_env(:tupee, :rate_limit_tick_sec)
  end

  def get_max_user_message_in_tick do
    Application.get_env(:tupee, :max_user_message_in_tick)
  end

  def get_max_total_message_in_tick do
    Application.get_env(:tupee, :total_message_in_tick)
  end

  def sec_to_time_messasge(sec) do
    cond do
      sec < 60 -> "#{sec} sec"
      sec < 120 -> "1 minute"
      sec < 3600 -> "#{round(sec / 60)} minutes"
      sec < 7200 -> "1 hour"
      sec < 86400 -> "#{round(sec / 3600)} hours"
      sec < 172800 -> "1 day"
      sec < 604800 -> "#{round(sec / 86400)} days"
      sec < 1209600 -> "1 week"
      sec < 2419200 -> "#{round(sec / 604800)} weeks"
      sec < 4838400 -> "1 month"
      sec < 29030400 -> "#{round(sec / 2419200)} months"
      sec < 58060800 -> "1 year"
      sec < (13_700_000_000 * 29030400) -> "#{round(sec / 29030400)} years"
      sec > (13_700_000_000 * 29030400) -> "longer than universe exists (congrats!)"
    end
  end
end
