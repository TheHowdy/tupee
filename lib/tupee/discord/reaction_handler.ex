defmodule Tupee.Discord.ReactionHandler do
  alias Nostrum.Api
  alias Nostrum.Cache.Me
  alias Nostrum.Struct.Embed
  alias Tupee.Discord.Chat

  use Agent

  def start_link(_) do
    Agent.start_link(fn -> %{} end, name: __MODULE__)
  end

  def add_continuation_handler(title, emoji, callback) do
    Agent.update(__MODULE__, fn state ->
      Map.put(state, {title, emoji}, callback)
    end)
  end

  def handle_reaction(reaction_data) do
    {:ok, msg} =
      Api.get_channel_message(reaction_data.channel_id, reaction_data.message_id)

    if should_respond?(msg) do
      callback = callback_of(reaction_data)

      if callback do
        response = callback.(reaction_data)
        Chat.reply(response, msg)
        Api.delete_all_reactions(reaction_data.channel_id, reaction_data.message_id)
      end
    end
  end

  def should_respond?(msg) do
    msg.author().id() == Me.get().id()
    and length(msg.embeds()) != 0
    and length(msg.reactions) == 1
    and List.first(msg.reactions).count == 2
  end

  def callback_of(reaction_data) do
    [%Embed{title: title} | _] = msg.embeds()
    emoji = reaction_data.emoji.name
    Agent.get(__MODULE__, fn state -> state[{title, emoji}] end)
  end
end
