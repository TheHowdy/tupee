defmodule Tupee.Discord.Chat do
  @moduledoc false
  alias Nostrum.Api
  alias Nostrum.Struct.Embed
  alias Tupee.Discord.ReactionHandler
  alias Tupee.{Util, Text}

  def reply(response, msg) do
    channel = msg.channel_id()

    case response do
      {:response, embed, [con_emoji: emoji, con_callback: callback]} ->
        msg = Api.create_message!(channel, embed: embed)
        Api.create_reaction!(channel, msg.id(), emoji)
        ReactionHandler.add_continuation_handler(embed.title, emoji, callback)
        :ok

      {:response, responses, []} when is_list(responses) ->
        Enum.each(responses, fn response ->
          Api.create_message!(channel, embed: response)
        end)
        :ok

      {:response, response, []} ->
        Api.create_message!(channel, embed: response)
        :ok

      :ignore ->
        :ignore
    end
  end

  def new_embed, do: %Embed{}

  def response(response, opts \\ []) do
    {:response, response, opts}
  end

  def error_response(title, message) do
    compose_response(title, message, :dark_red)
  end

  def success_response(embeds, opts \\ [])

  def success_response(embeds, opts) when is_list(embeds) do
    add_color = &(put_color(&1, Util.color(:dark_gold)))

    Enum.map(embeds, add_color) |> response(opts)
  end

  def success_response(title, message) when is_bitstring(title) do
    compose_response(title, message, :dark_gold)
  end

  def success_response(embed, opts) do
    put_color(embed, Util.color(:dark_gold))
    |> response(opts)
  end

  defp compose_response(title, message_id, color) when is_atom(message_id) do
    text = Text.get_random_text(message_id)
    compose_response(title, text, color)
  end

  defp compose_response(title, message, color) do
    new_embed()
    |> put_title(title)
    |> put_description(message)
    |> put_color(Util.color(color))
    |> response(embed)
  end
end
