defmodule Tupee.Discord do
  @moduledoc false
  alias Tupee.Discord.ReactionHandler
  alias Tupee.Discord.RateLimiter
  alias Tupee.Discord.Chat
  alias Tupee.Commands.CommandManager
  alias Tupee.Text

  use Nostrum.Consumer

  def start_link do
    Consumer.start_link(__MODULE__)
  end

  def handle_event({:MESSAGE_CREATE, msg, state}) do
    case RateLimiter.accept_message?(msg.author()) do
      :ok ->
        CommandManager.dispatch_command(msg, state)

      :total_limit_reached ->
        Text.get_random_text(:total_spam_protection)
        |> &(Chat.success_response("Hol' up.", &1))
        |> Chat.reply(msg)

      :blocked ->
        :ignore

      :on_blocked ->
        Text.get_random_text(:user_spam_protection)
        |> &(Chat.success_response("Hol' up.", &1))
        |> Chat.reply(msg)
    end
  end

  def handle_event({:MESSAGE_REACTION_ADD, reaction, _state}) do
    ReactionHandler.handle_reaction(reaction)
  end

  # Necessary so all events are handled somehow
  def handle_event(_event) do
    :noop
  end
end
