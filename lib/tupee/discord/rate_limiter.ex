defmodule Tupee.Discord.RateLimiter do
  @moduledoc false
  alias Nostrum.Struct.User
  alias Tupee.Util

  use GenServer

  def start_link() do
    GenServer.start_link(__MODULE__, %{})
  end

  def init(state) do
    schedule_tick()
    {:ok, state}
  end

  def accept_message?(user) do
    GenServer.call(__MODULE__, {:accept?, user})
  end

  def handle_call({:accept?, user}, _from, state) do
    user = state[id]

    state = increment_total(state)

    cond do
      max_total_messages_reached?(state.total) ->
        {:reply, :total_limit_reached, increment_ticks(state, id)}

      user == nil ->
        state = Map.put(state, id, new_user_state())
        {:reply, :ok, state}

      blocked?(user) ->
        {:reply, :on_blocked, state}

      max_user_messages_reached?(user) ->
        state =
          state
          |> increment_ticks(id)
          |> increase_blocked_time(id)
        {:reply, :blocked, state}

      true ->
        {:reply, :ok, increment_ticks(state, id)}
    end
  end

  def handle_info(:tick, state) do
    schedule_tick()

    state =
      for {id, user} <- state, into: %{} do
        %{blocked_for: blocked_for, clear_history_ticks: clear_history_ticks} = user

        updated_user = cond do
          blocked?(user) ->
            %{user | blocked_for: blocked_for - 1, messages_in_tick: 0}

          punished_recently?(user) ->
            %{user | clear_history_ticks: clear_history_ticks - 1, messages_in_tick: 0}
        end

        {id, updated_user}
      end

    {:noreply, Map.put(state, :total, 0)}
  end

  defp max_total_messages_reached?(total_msgs) do
    total_msgs >= Util.get_max_total_message_in_tick()
  end

  defp max_user_messages_reached?(%{messages_in_tick: user_msgs}) do
    user_msgs >= Util.get_max_user_message_in_tick()
  end

  defp blocked?(%{blocked_for: blocked_for}) do
    blocked_for > 0
  end

  defp punished_recently?(%{clear_history_ticks: clear_history_ticks}) do
    clear_history_ticks > 0
  end

  defp new_user_state do
    %{
      messages_in_tick: 1,
      blocked_for: 0,
      last_blocked_for: 0,
      clear_history_ticks: 0
    }
  end

  defp increment_ticks(state, id) do
    Map.update!(state, id, fn user ->
      user[:message_in_tick] = user[:messages_in_tick] + 1
    end)
  end

  defp increase_blocked_time(state, id) do
    Map.update!(state, id, fn user ->

      blocked_for = if punished_recently?(user) do
        user[:last_blocked_for]
      else
        user[:blocked_for]
      end

      new_blocked_for = :math.pow(blocked_for + 1, blocked_for)

      %{user | blocked_for: blocked_for,
               last_blocked_for: blocked_for,
               clear_history_ticks: blocked_for}
    end)
  end

  defp increment_total(state) do
    Map.update!(state, :total, &(&1 + 1))
  end

  defp schedule_tick do
    sec = Util.get_rate_limit_tick_sec()
    Process.send_after(self(), :tick, sec * 1000)
  end
end
