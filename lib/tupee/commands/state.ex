defmodule Tupee.Commands.State do
  @moduledoc false

  @enforce_keys [:msg, :args, :author, :server_state]
  defstruct [:msg, :args, :author, :server_state]
end
