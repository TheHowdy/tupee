defmodule Tupee.Commands.CurrenciesCommand do
  @moduledoc false
  alias Tupee.Economy.{Currency, Rate}
  alias Tupee.Chart

  import Tupee.Economy.Transaction
  import Tupee.Discord.Chat
  import Nostrum.Struct.Embed

  @command "currencies"
  @help "Lists all currencies and plots the whole economy."
  @syntax []

  @reply_to_emoji "\xF0\x9F\x93\x88"

  def init do
    Tupee.Commands.CommandManager.register_command(
      module: __MODULE__,
      command: @command,
      help: @help,
      syntax: @syntax
    )
  end

  def handle(_state) do
    messages =
      Currency.get_currencies()
      |> Enum.map(&format_currency/1)

    main_embed =
      new_embed()
      |> put_title("Economy & Currencies")
      |> put_description("""
      Welcome to a random overview of the current economic state of affairs.
      Press :chart_with_upwards_trend: for more fancy charts.
      """)
      |> put_image(Chart.plot_economy(:day))
      |> put_messages(messages)

    success_response(main_embed, con_emoji: @reply_to_emoji, con_callback: &handle_emoji/1)
  end

  defp handle_emoji(_) do
    month_overview_embed =
      new_embed()
      |> put_title("Currencies in the last 30days")
      |> put_image(Chart.plot_economy(:month))

    market_worth_overview =
      new_embed()
      |> put_title("Market Worth Overview")
      |> put_image(Chart.plot_market_worth())

    success_response([month_overview_embed, market_worth_overview])
  end

  defp format_currency(%Currency{code: code, name: name}) do
    base = Tupee.Util.get_base_currency_code()
    rate = Rate.rate_of(code)
    {":bar_chart: #{name}(`#{code}`)", "```fix\n1#{code} = #{rate}#{base}```"}
  end

  defp put_messages(embed, messages) do
    Enum.reduce(messages, embed, fn ({title, desc}, embed) ->
      put_field(embed, title, desc, true)
    end)
  end
end
