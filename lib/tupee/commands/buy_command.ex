defmodule Tupee.Commands.BuyCommand do
  @moduledoc false
  alias Tupee.Commands.State
  alias Tupee.Economy
  alias Tupee.Text

  import Tupee.Discord.Chat

  @command "buy"
  @help "Buys currency for other currency, e.g. 'buy 30USD for HZT'"
  @syntax [:amount, :currency, "for", :currency]

  def init do
    Tupee.Commands.CommandManager.register_command(
      module: __MODULE__,
      command: @command,
      help: @help,
      syntax: @syntax
    )
  end

  def handle(state) do
    %State{
      author: src_user,
      args: [amount, dest_currency, _, src_currency]
    } = state

    res = Economy.convert(src_user, amount, src_currency, dest_currency)

    case res do
      {:ok, amount} ->
        text = Text.get_random_text(:command_buy_success,
          amount: amount, code: src_currency.code)
        success_response("Success", text)

      {:error, :bot_sender} ->
        success_response("Success", :command_buy_sender_bot)

      {:error, :not_enough_money} ->
        error_response("Not enough money", :command_buy_not_enough_money)
    end
  end
end
