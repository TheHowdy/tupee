defmodule Tupee.Commands.StatusCommand do
  @moduledoc false
  import Tupee.Discord.Chat

  @command "status"
  @help "Prints the status of the bot."
  @syntax []

  def init do
    Tupee.Commands.CommandManager.register_command(
      module: __MODULE__,
      command: @command,
      help: @help,
      syntax: @syntax
    )
  end

  def handle(_state), do: success_response("Status", :command_status)
end
