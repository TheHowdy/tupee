defmodule Tupee.Commands.NewCurrencyCommand do
  @moduledoc false
  import Tupee.Discord.Chat

  @command "newcurrency"
  @help """
  Creates a new currency with a custom name and initial exchange course. Costs 1 of the base currency.
  e.g. "newcurrency Kekse(KKS) 200" creates a new currency, where 1 KKS equals 200 of the base currency.
  """
  @syntax [:full_currency, :amount]

  def init do
    Tupee.Commands.CommandManager.register_command(
      module: __MODULE__,
      command: @command,
      help: @help,
      syntax: @syntax
    )
  end

  def handle(_state) do
  end
end
