defmodule Tupee.Commands.PayCommand do
  @moduledoc false
  alias Tupee.Commands.State
  alias Tupee.Economy
  alias Tupee.Text

  import Tupee.Discord.Chat

  @command "pay"
  @help "Pays the specified user some currency, e.g. 'pay @Bob 30EUR'"
  @syntax [:user, :amount, :currency]

  def init do
    Tupee.Commands.CommandManager.register_command(
      module: __MODULE__,
      command: @command,
      help: @help,
      syntax: @syntax
    )
  end

  def handle(state) do
    %State{
      msg: msg,
      author: src_user,
      args: [_, amount, currency]
    } = state

    dest_user = msg.mentions() |> List.first()

    res = Economy.pay(src_user, dest_user, currency, amount)

    case res do
      :ok ->
        success_response("Success", :command_pay_success)

      {:error, :not_enough_money} ->
        username = src_user.username()
        text = Text.get_random_text(:command_pay_not_enough_money, user: username)
        error_response("Not enough money", text)

      {:error, :self_recipient} ->
        error_response("Sender is Recipient", :command_pay_recipient_self)

      {:error, :bot_sender} ->
        error_response("Sender is a bot", :command_pay_sender_bot)

      {:error, :bot_recipient} ->
        error_response("Recipient not found", :command_pay_recipient_not_found)
    end
  end
end
