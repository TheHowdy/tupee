defmodule Tupee.Commands.HelpCommand do
  @moduledoc false
  alias Tupee.Commands.CommandManager

  import Tupee.Discord.Chat
  import Nostrum.Struct.Embed

  @command "help"
  @help "Prints a list of commands."
  @syntax []

  def init do
    Tupee.Commands.CommandManager.register_command(
      module: __MODULE__,
      command: @command,
      help: @help,
      syntax: @syntax
    )
  end

  def handle(_state) do
    new_embed()
    |> put_title("Help")
    |> put_thumbnail(Tupee.Util.get_bot_avatar())
    |> put_description("The following commands are supported:")
    |> put_command_info()
    |> success_response()
  end

  defp put_command_info(embed) do
    commands = CommandManager.command_list()

    embed = Enum.reduce(commands, embed, fn (command, embed) ->
     {title, desc} = format_command(command)
      put_field(embed, title, desc)
    end)
  end

  defp format_command(command) do
    help = CommandManager.help_message(command)
    prefix = Tupee.Util.get_prefix()
    {":point_right: #{prefix}#{command}", "```json\n#{help}```"}
  end
end
