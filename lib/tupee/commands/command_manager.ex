defmodule Tupee.Commands.CommandManager do
  @moduledoc false
  alias Tupee.Util.Syntax
  alias Tupee.Commands.State
  alias Tupee.Discord.Chat
  alias Tupee.Text

  use Agent

  def start_link(_) do
    Agent.start_link(fn -> %{} end, name: __MODULE__)
  end

  def dispatch_command(msg, server_state) do
    case Syntax.check_syntax(msg.content) do
      {:ok, {command, args}} ->
        state = %State{args: args, msg: msg, author: msg.author(), server_state: server_state}
        module(command) |> apply(:handle, [state])

      {:error, :not_a_command} ->
        :ignore

      {:error, :command_not_found} ->
        :ignore

      {:error, {:wrong_length, command}} ->
        text = Text.get_random_text(:syntax_wrong_length, help: help_message(command))
        Chat.error_response("Wrong Syntax", text)

      {:error, {:errors, errors}} ->
        text = Text.get_random_text(:syntax_errors, errors)
        Chat.error_response("Wrong Syntax", text)
    end
    |> Chat.reply(msg)
  end

  def register_command(module: module, command: command, help: help, syntax: syntax) do
    Agent.update(__MODULE__, fn state ->
      Map.put(state, command, %{
        module: module,
        help: help,
        syntax: syntax
      })
    end)
  end

  def help_messages, do: extract_all(:help)

  def help_message(command), do: extract_single(command, :help)

  def syntax_definitions, do: extract_all(:syntax)

  def syntax_definition(command), do: extract_single(command, :syntax)

  def module(command), do: extract_single(command, :module)

  def command_exists?(command) do
    Agent.get(__MODULE__, &(Map.has_key?(&1, command)))
  end

  def command_list do
    Agent.get(__MODULE__, &(Map.keys(&1)))
  end

  defp extract_all(field) do
    Agent.get(__MODULE__, fn state -> state end)
    |> Enum.map(fn {command, values} -> {command, Map.fetch!(values, field)} end)
    |> Map.new()
  end

  defp extract_single(command, field) do
    case Agent.get(__MODULE__, &(&1[command])) do
      nil -> nil
      values -> Map.fetch!(values, field)
    end
  end
end
