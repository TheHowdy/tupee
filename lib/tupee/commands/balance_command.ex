defmodule Tupee.Commands.BalanceCommand do
  @moduledoc false
  alias Nostrum.Struct.User
  alias Tupee.Economy.{Balance,Currency,Rate}
  alias Tupee.Commands.State

  import Tupee.Economy.Transaction
  import Tupee.Discord.Chat
  import Nostrum.Struct.Embed

  @command "balance"
  @help "Displays the current users balance."
  @syntax []

  @base_currency_rate 1

  def init do
    Tupee.Commands.CommandManager.register_command(
      module: __MODULE__,
      command: @command,
      help: @help,
      syntax: @syntax
    )
  end

  def handle(%State{author: src_user}) do
    Balance.give_base_currency_if_needed(src_user)

    messages =
      src_user.id()
      |> Balance.get_balances()
      |> Enum.map(&format_currency/1)

    avatar = User.avatar_url(src_user)

    embed =
      new_embed()
      |> put_title("Balance of #{src_user.username()}")
      |> put_thumbnail(avatar)
      |> put_messages(messages)

    success_response(embed)
  end

  defp format_currency(%Balance{user_code: {_, code}, balance: balance}) do
    base = Tupee.Util.get_base_currency_code()
    rate = Rate.rate_of(code)

    body = format_currency_body(code, base, balance, rate)
    %Currency{name: name} = Currency.get_currency(code)

    {"#{name}(`#{code}`)", "```fix\n#{info}```"}
  end

  defp format_currency_body(code, balance, rate)
       when rate == @base_currency_rate, do: "#{balance}#{code}"

  defp format_currency_body(code, balance, rate),
       do: "#{balance}#{code} = (#{rate * balance}#{Util.get_base_currency_code()})"


  defp put_messages(embed, messages) do
    Enum.reduce(messages, embed, fn ({title, desc}, embed) ->
      put_field(embed, title, desc)
    end)
  end
end
