defmodule Tupee.Application do
  @moduledoc false
  use Application

  def start(_type, _args) do
    children = [
      init_db(),
      Tupee.Discord,
      Tupee.Discord.ReactionHandler,
      Tupee.Commands.CommandManager,
      init_commands()
    ]

    opts = [strategy: :one_for_one, name: Tupee.Supervisor]
    Supervisor.start_link(children, opts)
  end

  def init_db do
    callback = fn ->
      tables = [
        Tupee.Economy.Balance,
        Tupee.Economy.Currency,
        Tupee.Economy.Rate
      ]

      Enum.each(tables, &Memento.Table.create!/1)

      Tupee.Economy.Currency.create_base_currency()
    end

    Supervisor.child_spec({Task, callback}, id: :init_db)
  end

  def init_commands do
    callback = fn ->
      commands = [
        Tupee.Commands.BalanceCommand,
        Tupee.Commands.BuyCommand,
        Tupee.Commands.CurrenciesCommand,
        Tupee.Commands.HelpCommand,
        Tupee.Commands.NewCurrencyCommand,
        Tupee.Commands.PayCommand,
        Tupee.Commands.StatusCommand
      ]

      Enum.each(commands, &(apply(&1, :init, [])))
    end

    Supervisor.child_spec({Task, callback}, id: :init_commands)
  end
end
